module gitlab.com/xiayesuifeng/goblog-plugins/sitemap

go 1.17

require (
	github.com/gin-gonic/gin v1.6.1
	github.com/jinzhu/gorm v1.9.12
	gitlab.com/xiayesuifeng/goblog/plugins v0.0.0-20200411064256-3a44ccbca445
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator/v10 v10.2.0 // indirect
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742 // indirect
	github.com/ugorji/go/codec v1.1.7 // indirect
	gitlab.com/xiayesuifeng/goblog/conf v0.0.0-20200409174536-bd13490867bd // indirect
	golang.org/x/sys v0.0.0-20200321134203-328b4cd54aae // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
