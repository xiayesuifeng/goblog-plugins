package sitemap

import (
	"encoding/xml"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/goblog/plugins"
	"strconv"
)

var Server *plugins.PluginServer

type Article struct {
	gorm.Model
}

type Sitemap struct {
	XMLName xml.Name `xml:"urlset"`
	Xmlns   string   `xml:"xmlns,attr"`

	URLs []URL `xml:"url"`
}

type URL struct {
	XMLName xml.Name `xml:"url"`
	Loc     string   `xml:"loc"`
	LastMod string   `xml:"lastmod,omitempty"`
}

func GetSitemap(ctx *gin.Context) {
	scheme := ctx.GetHeader("X-Forwarded-Proto")
	if scheme == "" {
		scheme = "http://"
	} else {
		scheme += "://"
	}

	blogUrl := scheme + ctx.Request.Host

	db := Server.DatabaseInstance()

	sitemap := &Sitemap{Xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9"}
	sitemap.URLs = append(sitemap.URLs, URL{Loc: blogUrl})

	var articles []Article

	db.Where("private = 0").Order("created_at DESC").Find(&articles)

	for _, article := range articles {
		url := blogUrl + "/article/" + strconv.Itoa(int(article.ID))
		sitemap.URLs = append(sitemap.URLs, URL{Loc: url, LastMod: article.UpdatedAt.Format("2006-01-02 15:04:05")})
	}

	ctx.XML(200, sitemap)
}
