package main

import (
	"encoding/json"
	"flag"
	"gitlab.com/xiayesuifeng/goblog/plugins"
	"html/template"
	"io/ioutil"
	"log"
	"os"
	"plugin"
	"strings"
)

var (
	path    = flag.String("p", "", "plugins path")
	version = flag.String("v", "", "goblog version")
)

const html = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>GoBlog 插件中心</title>
</head>
<body>
    <h2>主程序</h2>
    <hr>
    <div>
        <a href="goblog">goblog</a><br>
        <p>版本号：{{.CompatibleVersion}}</p>
    </div>
    <h2>插件 兼容的主程序版本号：{{.CompatibleVersion}}</h2>
    <hr>
    {{range .Plugins}}
    <a href="{{.PackageName}}.so">{{.PackageName}}.so</a><br>
    {{end}}
</body>
</html>`

type Plugins struct {
	CompatibleVersion string   `json:"compatibleVersion"`
	Plugins           []Plugin `json:"plugins"`
}

type Plugin struct {
	PackageName   string `json:"packageName"`
	PluginName    string `json:"pluginName"`
	PluginVersion string `json:"pluginVersion"`
}

func main() {
	infos, err := ioutil.ReadDir(*path)
	if err != nil {
		log.Panicln(err)
	}

	data := Plugins{CompatibleVersion: *version}

	for _, info := range infos {
		if strings.HasSuffix(info.Name(), ".so") {
			name := info.Name()[:strings.Index(info.Name(), ".so")]

			p, err := plugin.Open(*path + "/" + info.Name())
			if err != nil {
				log.Panicln(err)
			}

			symbol, err := p.Lookup(strings.Title(name + "Plugin"))
			if err != nil {
				log.Panicln(err)
			}

			pl := symbol.(plugins.Plugins)

			tmp := Plugin{
				PackageName:   name,
				PluginName:    pl.GetPluginName(),
				PluginVersion: pl.GetPluginVersion(),
			}

			data.Plugins = append(data.Plugins, tmp)

		}
	}

	file, err := os.Create("plugins.json")
	if err != nil {
		log.Panicln(err)
	}
	defer file.Close()

	if err := json.NewEncoder(file).Encode(&data); err != nil {
		log.Panicln(err)
	}

	t, err := template.New("index").Parse(html)
	if err != nil {
		log.Panicln(err)
	}

	htmlFile, err := os.Create("index.html")
	if err != nil {
		log.Panicln(err)
	}
	defer htmlFile.Close()

	if err := t.Execute(htmlFile, &data); err != nil {
		log.Panicln(err)
	}
}

func init() {
	flag.Parse()
}
