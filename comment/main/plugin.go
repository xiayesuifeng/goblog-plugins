package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/goblog-plugins/comment"
	"gitlab.com/xiayesuifeng/goblog/plugins"
)

var CommentPlugin Plugin

type Plugin struct {
	server *plugins.PluginServer
}

func (p *Plugin) InitPlugins(server *plugins.PluginServer) error {
	p.server = server
	comment.Server = server

	return nil
}

func (p *Plugin) InitDatabase() error {
	db := p.server.DatabaseInstance()
	db.AutoMigrate(&comment.Comment{})

	return nil
}

func (p *Plugin) InitRouter(router *gin.RouterGroup) error {
	commentController := &comment.CommentController{}

	router.GET("/comment/:articleID", commentController.Gets)
	router.POST("/comment", commentController.Post)
	router.DELETE("/comment/:id", p.server.LoginMiddleware, commentController.Delete)

	return nil
}

func (p *Plugin) GetPluginName() string {
	return "评论"
}

func (p *Plugin) GetPluginVersion() string {
	return "0.0.1"
}
