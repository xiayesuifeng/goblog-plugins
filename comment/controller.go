package comment

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type CommentController struct{}

func (c *CommentController) Gets(ctx *gin.Context) {
	id, err := strconv.ParseUint(ctx.Param("articleID"), 10, 32)
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": "id must integer",
		})
	}

	ctx.JSON(200, gin.H{
		"code":     200,
		"comments": GetComments(uint(id)),
	})
}

func (c *CommentController) Post(ctx *gin.Context) {
	comment := Comment{}

	if err := ctx.ShouldBind(&comment); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
		return
	}

	if err := AddComment(comment); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
		})
	}
}

func (c *CommentController) Delete(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": "id must integer",
		})
		return
	}

	if err := DeleteComment(id); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
		})
	}
}
