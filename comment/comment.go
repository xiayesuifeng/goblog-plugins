package comment

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/goblog/plugins"
)

var Server *plugins.PluginServer

type Comment struct {
	gorm.Model
	ArticleID  uint      `json:"articleID,omitempty" binding:"required"`
	Username   string    `json:"username" binding:"required"`
	Email      string    `json:"email,omitempty"`
	Comment    string    `json:"comment" binding:"required" gorm:"type:text"`
	ParentID   uint      `json:"parentID,omitempty"`
	SubComment []Comment `json:"subComment" gorm:"foreignkey:ParentID"`
}

func subCommentConditions(db *gorm.DB) *gorm.DB {
	return db.
		Preload("SubComment", subCommentConditions).
		Select([]string{"id", "created_at", "updated_at", "username", "comment", "parent_id"}).
		Order("created_at DESC")
}

func GetComments(articleID uint) []Comment {
	db := Server.DatabaseInstance()

	var comments []Comment

	db.Select([]string{"id", "created_at", "updated_at", "username", "comment", "parent_id"}).
		Where("article_id=? AND parent_ID=0", articleID).
		Order("created_at DESC").
		Preload("SubComment", subCommentConditions).
		Find(&comments)

	return comments
}

func AddComment(comment Comment) error {
	db := Server.DatabaseInstance()

	return db.Create(&comment).Error
}

func DeleteComment(id int) error {
	db := Server.DatabaseInstance()

	db.Unscoped().Where("parent_id=?", id).Delete(&Comment{})

	return db.Unscoped().Delete(&Comment{}, id).Error
}
