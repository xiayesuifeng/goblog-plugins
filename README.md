# goblog-plugins

> 个人博客 `goblog` 后端插件库

[![pipeline status](https://gitlab.com/xiayesuifeng/goblog-plugins/badges/master/pipeline.svg)](https://gitlab.com/xiayesuifeng/goblog-plugins/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/xiayesuifeng/goblog-plugins)](https://goreportcard.com/report/gitlab.com/xiayesuifeng/goblog-plugins)
[![GoDoc](https://godoc.org/gitlab.com/xiayesuifeng/goblog-plugins?status.svg)](https://godoc.org/gitlab.com/xiayesuifeng/goblog-plugins)
[![Sourcegraph](https://sourcegraph.com/gitlab.com/xiayesuifeng/goblog-plugins/-/badge.svg)](https://sourcegraph.com/gitlab.com/xiayesuifeng/goblog-plugins)

## 插件列表

- 评论 - comment.so
- 友链 - links.so
- RSS订阅 - feed.so
- 站点地图 - sitemap.so

## 插件安装

打开 https://xiayesuifeng.gitlab.io/goblog-plugins

下载所需的插件并放到 `goblog` 所在目录下的 `plguins` 下