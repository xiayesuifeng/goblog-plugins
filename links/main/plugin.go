package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/goblog-plugins/links"
	"gitlab.com/xiayesuifeng/goblog/plugins"
)

var LinksPlugin Plugin

type Plugin struct {
	server *plugins.PluginServer
}

func (p *Plugin) InitPlugins(server *plugins.PluginServer) error {
	p.server = server
	links.Server = server

	return nil
}

func (p *Plugin) InitDatabase() error {
	db := p.server.DatabaseInstance()
	db.AutoMigrate(&links.Link{})

	return nil
}

func (p *Plugin) InitRouter(router *gin.RouterGroup) error {
	linksController := &links.LinksController{}

	router.GET("/links", linksController.Gets)
	router.GET("/links/:id", linksController.GetById)
	router.POST("/links", p.server.LoginMiddleware, linksController.Post)
	router.PUT("/links/:id", p.server.LoginMiddleware, linksController.Put)
	router.DELETE("/links/:id", p.server.LoginMiddleware, linksController.Delete)

	return nil
}

func (p *Plugin) GetPluginName() string {
	return "友情链接"
}

func (p *Plugin) GetPluginVersion() string {
	return "0.0.1"
}
