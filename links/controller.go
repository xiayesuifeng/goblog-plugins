package links

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

type LinksController struct{}

func (c *LinksController) Gets(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"code":  200,
		"links": GetLinks(),
	})
}

func (c *LinksController) GetById(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": "id must integer",
		})
		return
	}

	link, err := GetLinkById(id)
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    404,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
			"link": link,
		})
	}
}

func (c *LinksController) Post(ctx *gin.Context) {
	link := Link{}

	if err := ctx.ShouldBind(&link); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
		return
	}

	if err := AddLink(link); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
		})
	}
}
func (c *LinksController) Put(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": "id must integer",
		})
		return
	}

	link := Link{}

	if err := ctx.ShouldBind(&link); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
		return
	}

	link.ID = uint(id)

	if err := EditLink(link); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
		})
	}
}
func (c *LinksController) Delete(ctx *gin.Context) {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": "id must integer",
		})
		return
	}

	if err := DeleteLink(id); err != nil {
		ctx.JSON(200, gin.H{
			"code":    400,
			"message": err.Error(),
		})
	} else {
		ctx.JSON(200, gin.H{
			"code": 200,
		})
	}
}
