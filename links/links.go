package links

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/xiayesuifeng/goblog/plugins"
)

var Server *plugins.PluginServer

type Link struct {
	gorm.Model
	Name string `json:"name" gorm:"unique" binding:"required"`
	Url  string `json:"url" gorm:"unique" binding:"required"`
}

func GetLinks() []Link {
	db := Server.DatabaseInstance()

	var links []Link

	db.Find(&links)

	return links
}

func GetLinkById(id int) (Link, error) {
	db := Server.DatabaseInstance()

	link := Link{}

	if db.First(&link, id).RecordNotFound() {
		return link, errors.New("link not found")
	}

	return link, nil
}

func AddLink(link Link) error {
	db := Server.DatabaseInstance()

	return db.Create(&link).Error
}

func EditLink(link Link) error {
	db := Server.DatabaseInstance()

	return db.Model(&link).Update(link).Error
}

func DeleteLink(id int) error {
	db := Server.DatabaseInstance()

	return db.Unscoped().Delete(&Link{}, id).Error
}
