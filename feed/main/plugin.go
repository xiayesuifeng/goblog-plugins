package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xiayesuifeng/goblog-plugins/feed"
	"gitlab.com/xiayesuifeng/goblog/plugins"
)

var FeedPlugin Plugin

type Plugin struct {
	server *plugins.PluginServer
}

func (p *Plugin) InitPlugins(server *plugins.PluginServer) error {
	p.server = server
	feed.Server = server

	return nil
}

func (p *Plugin) InitDatabase() error {
	return nil
}

func (p *Plugin) InitRouter(router *gin.RouterGroup) error {
	router.GET("/feed", feed.GetFeed)
	router.GET("/feed/:type", feed.GetFeed)

	return nil
}

func (p *Plugin) GetPluginName() string {
	return "RSS订阅"
}

func (p *Plugin) GetPluginVersion() string {
	return "0.0.3"
}
