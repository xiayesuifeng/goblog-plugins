package feed

import (
	"github.com/gin-gonic/gin"
	"github.com/gorilla/feeds"
	"github.com/jinzhu/gorm"
	"github.com/russross/blackfriday"
	"gitlab.com/xiayesuifeng/goblog/plugins"
	"io/ioutil"
	"strconv"
	"time"
)

var Server *plugins.PluginServer

type Article struct {
	gorm.Model
	Title   string
	Tag     string
	Uuid    string
	Private bool
}

func GetFeed(ctx *gin.Context) {
	param := ctx.Param("type")

	blogUrl := "https://" + ctx.Request.Host

	db := Server.DatabaseInstance()

	feed := &feeds.Feed{
		Title:   Server.Config.Name,
		Link:    &feeds.Link{Href: blogUrl},
		Created: time.Now(),
	}

	var articles []Article

	db.Order("created_at DESC").Find(&articles)

	for _, article := range articles {
		if article.Private {
			continue
		}

		url := blogUrl + "/article/" + strconv.Itoa(int(article.ID))

		item := &feeds.Item{
			Id:      url,
			Title:   article.Title,
			Link:    &feeds.Link{Href: url},
			Created: article.CreatedAt,
			Updated: article.UpdatedAt,
		}

		if param == "rss" {
			item.Description = mdToHTML(article.Uuid)
		} else {
			item.Content = mdToHTML(article.Uuid)
		}

		feed.Add(item)
	}

	result := ""
	switch param {
	case "rss":
		result, _ = feed.ToRss()
	case "json":
		result, _ = feed.ToJSON()
	default:
		result, _ = feed.ToAtom()
	}

	ctx.String(200, result)
}

func mdToHTML(uuid string) string {
	md, err := ioutil.ReadFile(Server.Config.DataDir + "/article/" + uuid + ".md")
	if err != nil {
		return ""
	}

	html := blackfriday.MarkdownBasic(md)
	return string(html)
}
